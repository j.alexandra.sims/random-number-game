# Random Number Game

User prompted to guess a number between 1 and 10. User prompted to continue guessing until the correct answer is typed in. Program then displays the number of tries and the correctly guessed number.